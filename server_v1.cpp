#include <iostream>
#include <string>
#include <map>
#include <cstring>
#include <unistd.h>
#include <arpa/inet.h>

static int PORT = 6668;

int serverSocket;

std::map<int, sockaddr_in> current;

std::string NormalizedIPString(sockaddr_in addr) {
    char host[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(addr.sin_addr), host, INET_ADDRSTRLEN);

    uint16_t port = ntohs(addr.sin_port);

    return std::string(host) + ":" + std::to_string(port);
}

void SendResponse(sockaddr_in addr, sockaddr_in receiver) {
    std::string msg = NormalizedIPString(addr);

    sendto(serverSocket, msg.c_str(), msg.length(), 0, (sockaddr*)&receiver, sizeof(receiver));
}

int main() {
    std::cout << "starting..." << std::flush;	
    serverSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (serverSocket == -1) {
        perror("Socket creation failed");
        return 1;
    }

    sockaddr_in sockAddr;
    sockAddr.sin_port = htons(PORT);
    sockAddr.sin_family = AF_INET;
    sockAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(serverSocket, (sockaddr*)&sockAddr, sizeof(sockAddr)) == -1) {
        perror("Bind failed");
        return 1;
    }

    int val = 64 * 1024;
    if (setsockopt(serverSocket, SOL_SOCKET, SO_SNDBUF, &val, sizeof(val)) == -1) {
        perror("Setsockopt SO_SNDBUF failed");
    }
    if (setsockopt(serverSocket, SOL_SOCKET, SO_RCVBUF, &val, sizeof(val)) == -1) {
        perror("Setsockopt SO_RCVBUF failed");
    }

    while (true) {
        sockaddr_in clientAddr;
        socklen_t clientSize = sizeof(clientAddr);

        char buffer[1024];
        int bufferLength = 1024;

        int iResult = recvfrom(serverSocket, buffer, bufferLength, 0, (sockaddr*)&clientAddr, &clientSize);

        if (iResult > 0) {
            try {
                int id = std::stoi(std::string(buffer, buffer + iResult));

                if (current.find(id) != current.end()) {
                    sockaddr_in other = current[id];

                    SendResponse(other, clientAddr);
                    SendResponse(clientAddr, other);

                    std::cout << "Linked" << std::endl << "   ID: " << id <<  std::flush
                              << "   Endpoint1: " << NormalizedIPString(clientAddr) << std::flush
                              << "   Endpoint2: " << NormalizedIPString(other) << std::flush;

                    current.erase(id);
                }
                else {
                    current.insert(std::make_pair(id, clientAddr));

                    std::cout << "Registered" << std::endl << "   ID: " << id << std::flush
                              << "   Endpoint: " << NormalizedIPString(clientAddr)  << std::flush;
                }
            }
            catch (...) {}
        }
    }

    close(serverSocket);
    return 0;
}

