#pragma once
#include <WinSock2.h>
#include <iostream>
#include <vector>
#include <string>
#include <Ws2tcpip.h>

#include <iomanip>    // for std::setfill and std::setw
#include <sstream>
#include "json.hpp"
#pragma comment(lib, "ws2_32.lib") // Link with the Winsock library



#define NUM_OF_DIGITS_OF_MSG_SIZE 200
#define ERROR_CODE_MSG 500
#define SUCCESS_CODE_MSG 100

using namespace std;


class TCP_Helper
{
public:

    TCP_Helper(SOCKET clientSocket);

    std::string recvMsg()
    {
        int msg_size = 0;
        char size_buffer[NUM_OF_DIGITS_OF_MSG_SIZE];
        
        recv(this->_clientSocket, size_buffer, NUM_OF_DIGITS_OF_MSG_SIZE,0); //  --_  
                                                                             //     |
        std::string size_str = "";                                           //     |
        for (int i = 0; i < NUM_OF_DIGITS_OF_MSG_SIZE; i++)                  //     |
        {                                                                    //      > recive size of the msg
            size_str += size_buffer[i];                                      //     |
        }                                                                    //     |
        msg_size = std::stoi(size_str);                                      //     |
                                                                             //  __-
        char* msg_buffer = new char[msg_size + 1];

        //recive msg
        recv(this->_clientSocket, msg_buffer, msg_size, 0);
        msg_buffer[msg_size] = '\0';
        std::string resualt = std::string(msg_buffer);
        std::cout << "\033[33m";
        cout << "recieve msg: ";
        std::cout << "\033[0m";
        cout << resualt << endl;
        delete[] msg_buffer;
        return resualt;
    }

    void sendMsg(std::string msg)
    {
        int desiredWidth = NUM_OF_DIGITS_OF_MSG_SIZE; //the amount of digits of the len of the turent file
        size_t msg_size = msg.size();

        std::stringstream formattedStr;
        formattedStr << std::setfill('0') << std::setw(desiredWidth) << msg_size;

        std::string size_msg_with_zero = formattedStr.str();

        std::string finishMsg = size_msg_with_zero + msg;
        std::cout << "\033[33m";
        std::cout << "send: ";
        std::cout << "\033[0m";
        std::cout << finishMsg << std::endl;
        send(this->_clientSocket, finishMsg.c_str(), finishMsg.size(), 0);
    }

    
    
    int getAnswer(std::string json_answer)
    {
        int code = 0;
        try
        {
            nlohmann::json json_data = nlohmann::json::parse(json_answer);
            code = json_data["code"];
        }
        catch (const std::exception& e)
        {
            std::cerr << e.what();
        }
        // ANSI escape code for grey text
        std::cout << "\033[90m";
        std::cout << "D: Answer msg code: ";
        // Reset to normal text (to avoid affecting subsequent text)
        std::cout << "\033[0m";
        std::cout << "\033[33m";
        std::cout << code << std::endl;
        std::cout << "\033[0m";
        if (code == 500)
        {
            try
            {
                nlohmann::json json_data = nlohmann::json::parse(json_answer);
                std::string error = json_data["data"];
                std::cout << "\033[31m";
                std::cout << "Error: " << error << std::endl;
                std::cout << "\033[0m";
            }
            catch (const std::exception& e)
            {
                std::cerr << e.what();
            }
        }
        else
        {
            nlohmann::json json_data = nlohmann::json::parse(json_answer);
            std::string data = json_data["data"];

            if (data == "")
            {
                data = "empty";
            }

            // ANSI escape code for grey text
            std::cout << "\033[90m";
            std::cout << "D: msg data: " << data << std::endl;
            // Reset to normal text (to avoid affecting subsequent text)
            std::cout << "\033[0m";
        }
        return code;
    }

    void sendMsgError(string error)
    {
        vector<string> error_vect;
        error_vect.push_back(error);
        sendMsgCode(ERROR_CODE_MSG, error_vect);
    }

    void sendMsgCode(int code, std::vector<std::string> data)
    {
        int vectorSize = data.size();
        nlohmann::json json_msg;
        json_msg["code"] = code;
        if (vectorSize > 1 && vectorSize % 2 == 0)
        {
            nlohmann::json json_data;

            for (auto it = data.begin(); it != data.end(); it = it+ 2)
            {
                json_data[*it] = *(it + 1);
            }

            std::string json_string = json_data.dump();

            json_msg["data"] = json_string;
        }
        else if (vectorSize == 1)
        {
            json_msg["data"] = *(data.begin());
        }
        else
        {
            json_msg["data"] = "";
        }

        std::string json_string = json_msg.dump();
        sendMsg(json_string);
    }


    void sendMsgSuccess()
    {
        vector<string> void_vect;
        sendMsgCode(SUCCESS_CODE_MSG, void_vect);
    }
private:
    SOCKET _clientSocket;
};


