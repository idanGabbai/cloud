#pragma once
#include "Room.h"


class roomManager
{
public:
	roomManager();
	~roomManager();

	bool createRoom(const std::string roomName, const std::string room_password, client* admin);
	std::string joinRoom(const std::string roomName, const std::string room_password,const std::string UDP_password, client* client);
	std::string joinRoomCheck(const std::string roomName, const std::string room_password);

	void leaveRoom(const std::string roomName, client* client);
	
	//getters
	Room* getRoom(const std::string roomName) const;
	std::vector<Room*> getRooms() const;

private:
	std::vector<Room*> m_rooms;

	void closeRoom(std::string roomName);
};
