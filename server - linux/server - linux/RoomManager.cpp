#include "RoomManager.h"

roomManager::roomManager()
{
	this->m_rooms.clear();
}

roomManager::~roomManager()
{
	
}

bool roomManager::createRoom(const std::string roomName, const std::string room_password, client* admin)
{
	//check if the room name is already exist
	for (auto it = this->m_rooms.begin(); it != this->m_rooms.end(); it++)
	{
		if ((*it)->getName() == roomName)
		{
			return false;
		}
	}

	Room* newRoom = new Room(admin, roomName, room_password);

	this->m_rooms.push_back(newRoom);

	return true;
}

std::string roomManager::joinRoomCheck(const std::string roomName, const std::string room_password)
{
	for (auto it = this->m_rooms.begin(); it != this->m_rooms.end(); it++)
	{
		if ((*it)->getName() == roomName)
		{
			if ((*it)->getPassword() == room_password) 
			{
				return "success";
			}
			else
			{
				return "worng password.";
			}
		}
	}
	return "dont exist room with this name.";
}

// send "success" if its success
// and error string if not

std::string roomManager::joinRoom(const std::string roomName, const std::string room_password,const std::string UDP_password, client* client)
{
	for (auto it = this->m_rooms.begin(); it != this->m_rooms.end(); it++)
	{
		if ((*it)->getName() == roomName)
		{
			if ((*it)->getStatus() == 0)
			{
				if ((*it)->getPassword() == room_password)
				{
					if ((*it)->getUDP_password() == UDP_password)
					{
						(*it)->addClient(client);
						return "success";
					}
					else
					{
						return "UDP room password not valid";
					}
				}
				else
				{
					return "worng password.";
				}
			}
			else
			{
				return "the room already running";
			}
		}
	}
	return "dont exist room with this name.";

}

void roomManager::leaveRoom(const std::string roomName, client* client)
{
	for (auto it = this->m_rooms.begin(); it != this->m_rooms.end(); it++)
	{
		if ((*it)->getName() == roomName)
		{
			if (client == (*it)->getAdmin())
			{
				this->closeRoom(roomName); //if the admin of the room leave the room closing
				break;
			}
			else
			{
				(*it)->removeClient(client);
				break;
			}
		}
	}
}

Room* roomManager::getRoom(const std::string roomName) const
{
	for (auto it = this->m_rooms.begin(); it != this->m_rooms.end(); it++)
	{
		if ((*it)->getName() == roomName)
		{
			return (*it);
		}
	}
	return nullptr;
}

std::vector<Room*> roomManager::getRooms() const
{
	return this->m_rooms;
}

void roomManager::closeRoom(std::string roomName)
{
	for (auto it = this->m_rooms.begin(); it != this->m_rooms.end(); it++)
	{
		if ((*it)->getName() == roomName)
		{
			this->m_rooms.erase(it);
			
			//delete (*it);    //TODO //TODO
			break;
		}
	}
}

