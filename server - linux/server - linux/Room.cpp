#include "Room.h"


Room::Room(client* admin,std::string name, std::string password)
{
	this->_admin = admin;
	this->_clients.clear();
	this->_password = password;
	this->_name = name;
	this->_torrentsFile.clear();
	this->_status = false;
	// Seed the random number generator with the current time
	srand(static_cast<unsigned>(time(nullptr)));

	// Generate a random number with four digits
	int four_digit_number = rand() % 9000 + 1000;

	this->_UDP_password = std::to_string(four_digit_number);


	// Generate a random number with ten digits
	this->_G = rand() % 9000000000LL + 1000000000LL;

	
	// Generate a random number with ten digits
	this->_P = rand() % 9000000000LL + 1000000000LL;

}

Room::~Room()
{
	
}

void Room::addClient(client* client)
{
	this->_clients.push_back(client);
}

void Room::removeClient(client* client1)
{
	for (auto it = this->_clients.begin(); it != this->_clients.end(); it++)
	{
		if ((*it) == client1)
		{
			this->_clients.erase(it);
			break;
		}
	}
}

bool Room::addTorrentFile(turrent_file torrentFile,client* _client)
{
	if (_client == this->_admin)
	{
		this->_torrentsFile.push_back(torrentFile);
		return true;
	}
	else
	{
		return false;
	}
}

void Room::removeTurrentFiles()
{
	this->_torrentsFile.clear();
}

void Room::startRoom()
{
	this->_status = true;
}

void Room::stopRoom()
{
	this->_status = false;


}



std::string Room::getPassword() const
{
	return this->_password;
}

std::string Room::getName() const
{
	return this->_name;
}

std::vector<client*> Room::getClients() const
{
	return (this->_clients);
}

client* Room::getAdmin() const
{
	return this->_admin;
}

std::vector<turrent_file> Room::getTurrentFiles()
{
	return this->_torrentsFile;
}

std::string Room::getUDP_password() const
{
	return this->_UDP_password;
}

bool Room::getStatus() const
{
	return this->_status;
}

long long Room::getP()
{
	return this->_P;
}

long long Room::getG()
{
	return this->_G;
}
