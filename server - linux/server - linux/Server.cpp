
#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include <thread>

std::mutex mx1;

static int id = 0;

//UDP thread
void Server::receiveUDP_EndPoints() //thread
{
	// Initialize Winsock
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
		std::cerr << "UDP thread: Failed to initialize Winsock" << std::endl;
	}

	// Create a UDP socket
	SOCKET serverSocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (serverSocket == INVALID_SOCKET) {
		std::cerr << "UDP thread: Error creating socket: " << WSAGetLastError() << std::endl;
		WSACleanup();
	}

	// Set up the server address structure
	sockaddr_in serverAddress;
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_port = htons(UDP_SOCKET_PORT);

	// Bind the socket to the server address
	if (::bind(serverSocket, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) == SOCKET_ERROR) {
		std::cerr << "UDP thread: Error binding socket: " << WSAGetLastError() << std::endl;
		closesocket(serverSocket);
		WSACleanup();
	}

	std::cout << "UDP Server socket is listening on port " << UDP_SOCKET_PORT << std::endl;

	while (true) {
		// Receive data from clients
		char buffer[NUM_OF_ID_MSG_SIZE + 1];
		sockaddr_in clientAddress;
		int clientAddrLen = sizeof(clientAddress);
		int receivedBytes = recvfrom(serverSocket, buffer, NUM_OF_ID_MSG_SIZE, 0, (struct sockaddr*)&clientAddress, &clientAddrLen);

		if (receivedBytes == SOCKET_ERROR) {
			std::cerr << "UDP thread: Error receiving data: " << WSAGetLastError() << std::endl;
			closesocket(serverSocket);
			WSACleanup();
		}

		buffer[NUM_OF_ID_MSG_SIZE] = '\0';  // Null-terminate the received data

		// Print the received data and client information
		string endPointIp = inet_ntoa(clientAddress.sin_addr);
		string endPointPort = to_string((ntohs(clientAddress.sin_port)));
		string endPoint = endPointIp + ":" + endPointPort;
		if (stoi(buffer) == 0) //check if the packet is keep alive packet
		{
			std::cout << "\033[1;35mUDP thread:\033[0m Received from " << endPoint << " - keep alive packet"  << std::endl;
			continue;
		}
		else
		{
			std::cout << "\033[1;35mUDP thread:\033[0m Received from " << endPoint << " - " << buffer << std::endl;
		}
		int id = 0;
		try
		{
			id = stoi(buffer);
		}
		catch (...)
		{
			cout << "UDP thread: cant stoi the msg data to id(int)" << endl;
			continue;
		}

		this->clientsUDP[id] = endPoint;

	}

	// Close the socket and cleanup (unreachable in this example)
	closesocket(serverSocket);
	WSACleanup();
}


Server::Server()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

	this->_roomManager = new roomManager();

}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);

		if (this->_roomManager != nullptr)
		{
			delete this->_roomManager;
		}
	}
	catch (...) {}
}

void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "\033[33m";
	std::cout << "Listening on port ";
	std::cout << "\033[32m";
	std::cout << port << std::endl;
	std::cout << "\033[0m";

	thread thrd(&Server::receiveUDP_EndPoints, this);

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		acceptClient();
	}
}

void Server::acceptClient()
{

	// this accepts the client and create a specific socket from server to this client
	// the process will not continue until a client connects to the server
	SOCKET client_socket = accept(_serverSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	id++;
	this->clientsUDP[id] = "0.0.0.0";

	std::cout << "Client accepted." << std::endl;
	
	thread thrd(&Server::clientHandler, this,client_socket,id);

	thrd.detach();
	
}


void Server::clientHandler(SOCKET clientSocket,int id)
{
	client* Client = nullptr;
	Room* clientRoom = nullptr;
	string UDP_Endpoint = "0.0.0.0";

	string receivedMsg = "";
	structMsg msg_info;
	bool success = false;
	string buffer;

	try
	{
		TCP_Helper tcp_helper(clientSocket);

		//send the id to the client
		vector<string> data;
		data.push_back("id");
		data.push_back(to_string(id));
		tcp_helper.sendMsgCode(55, data);

		//waiting until the clietn connect to the UDP socket of the server
		while (UDP_Endpoint == "0.0.0.0")
		{
			Sleep(2000);
			UDP_Endpoint = this->clientsUDP[id];
		}
		tcp_helper.sendMsgSuccess();

		//login//
		do
		{
			//recive msg
			receivedMsg = tcp_helper.recvMsg();
			
			//json to the recieved msg
			msg_info = this->jsonMsg(receivedMsg);

			

			if (msg_info.code == LOGIN_CODE)
			{
				string username = "";
				try {
					nlohmann::json json_data = nlohmann::json::parse(msg_info.json_data);
					username = json_data["name"];
				}
				catch (const std::exception& e) {
					std::cerr << "Failed to deserialize JSON data: " << e.what() << std::endl;
				}

				//create client//
				client* client1 = new client(username,UDP_Endpoint,&tcp_helper);
				Client = client1;
			}
			

		} while (Client == nullptr);
		tcp_helper.sendMsgSuccess(); // send to the client that he login

		//----User requests switch----//
		while (true)
		{
			//recive msg
			receivedMsg = tcp_helper.recvMsg();

			//TODO: remove encryption

			//get msg code//
			msg_info = this->jsonMsg(receivedMsg);


			switch (msg_info.code)
			{

			case GIVE_ALL_ROOMS:
				this->give_all_rooms(tcp_helper);
				break;

			case CREATE_ROOM:
				success = this->create_room(msg_info.json_data, tcp_helper, Client);

				if (success)
				{
					nlohmann::json json_data1 = nlohmann::json::parse(msg_info.json_data);
					buffer = json_data1["room_name"];
					clientRoom = this->_roomManager->getRoom(buffer);
				}
				break;

			case JOIN_ROOM:
				success = this->joinRoom(msg_info.json_data, tcp_helper,Client);

				if (success)
				{
					nlohmann::json json_data1 = nlohmann::json::parse(msg_info.json_data);
					buffer = json_data1["room_name"];
					clientRoom = this->_roomManager->getRoom(buffer);
				}
				break;

			case JOIN_ROOM_CHECK:
				this->joinRoomCheck(msg_info.json_data, tcp_helper);
				break;

			case LEAVE_ROOM:
				if (clientRoom != nullptr)
				{
					this->_roomManager->leaveRoom(clientRoom->getName(), Client);
					clientRoom = nullptr;
					tcp_helper.sendMsgSuccess();
				}
				else
				{
					tcp_helper.sendMsgError("the user is not in any room");
				}

				break;

			case GIVE_USERS_IN_ROOM:
				if (clientRoom != nullptr)
				{
					give_all_users_in_room(tcp_helper, clientRoom);
				}
				else
				{
					tcp_helper.sendMsgError("the user is not in any room");
				}
				break;

			case GIVE_USERS_PUBLIC_KEY:
				if (clientRoom != nullptr)
				{
					give_users_public_key(tcp_helper, clientRoom);
				}
				else
				{
					tcp_helper.sendMsgError("the user is not in any room");
				}
				break;

			case PIECE_UPDATE:
				if (clientRoom != nullptr)
				{
					this->pieceUpdate(tcp_helper, clientRoom, Client, msg_info.json_data);
				}
				else
				{
					tcp_helper.sendMsgError("the user is not in any room");
				}
				break;

			case WANT_PIECE_FROM_ANOTHER_CLIENT:

				if (clientRoom != nullptr)
				{
					this->wantPieceFromAnotherClient(tcp_helper, clientRoom, Client, msg_info.json_data);
					

				}
				else
				{
					tcp_helper.sendMsgError("the user is not in any room");
				}
				break;

			case GIVE_MISSING_PIECES:
				if (clientRoom != nullptr)
				{
					if (Client == clientRoom->getAdmin())
					{
						this->sendMissingPieces(tcp_helper,msg_info.json_data, clientRoom);
					}
					else
					{
						tcp_helper.sendMsgError("only the admin have premission to this");
					}
				}
				else
				{
					tcp_helper.sendMsgError("the user is not in any room");
				}

				break;

			case START_ROOM:
				if (clientRoom != nullptr)
				{
					if (Client == clientRoom->getAdmin())
					{

						for (auto& it_client : clientRoom->getClients())
						{
							for (auto& turrent : clientRoom->getTurrentFiles())
							{
								(it_client)->addTorrent((turrent).file_name);
							}
						}

						clientRoom->startRoom();

						tcp_helper.sendMsgSuccess();
					}
					else
					{
						tcp_helper.sendMsgError("only the admin can start the room");
					}

				}
				else
				{
					tcp_helper.sendMsgError("the user is not in any room");
				}

				break;

			case STOP_ROOM:
				if (clientRoom != nullptr)
				{
					if (Client == clientRoom->getAdmin())
					{
						vector<string> temp;
						clientRoom->stopRoom();
						for (auto& it_client : clientRoom->getClients())
						{
							it_client->getTcpHelper()->sendMsgCode(88, temp);
						}

					}
					else
					{
						tcp_helper.sendMsgError("only the admin can start the room");
					}
				}
				else
				{
					tcp_helper.sendMsgError("the user is not in any room");
				}

				break;

			case UPLOAD_TORRENT_FILE:
				if (clientRoom != nullptr)
				{
					this->uploadTurrentFile(tcp_helper, clientRoom, Client, msg_info.json_data);
				}
				else
				{
					tcp_helper.sendMsgError("the user is not in any room");
				}
				break;

			case REMOVE_ROOM_TURRENT_FILES:
				if (clientRoom != nullptr)
				{
					if (clientRoom->getAdmin() == Client) //check if the client is the room admin
					{
						clientRoom->removeTurrentFiles();
						tcp_helper.sendMsgSuccess();
					}
					else
					{
						tcp_helper.sendMsgError("only the room admin can delete turrent files");
					}
				}
				else
				{
					tcp_helper.sendMsgError("the user is not in any room");
				}

				break;

			case GIVE_ROOM_TURRENT_FILES:
				if (clientRoom != nullptr)
				{
					vector<string> data;
					string buffer;
					int turrentsNum = clientRoom->getTurrentFiles().size();
					int answerCode = 0;

					if (turrentsNum > 0)
					{
						data.push_back("numOfTurrents");
						data.push_back(to_string(turrentsNum));
						tcp_helper.sendMsgCode(GIVE_ROOM_TURRENT_FILES, data);

						buffer = tcp_helper.recvMsg();
						answerCode = tcp_helper.getAnswer(buffer);

						if (answerCode != SUCCESS_CODE)
						{
							break;
						}

						vector<turrent_file> turrentFiles = clientRoom->getTurrentFiles();

						for (int i = 0; i < turrentsNum; i++)
						{
							data.clear();
							nlohmann::json json_data1;
							json_data1["file_name"] = turrentFiles[i].file_name;
							json_data1["file_len"] = turrentFiles[i].file_len;
							json_data1["piece_len"] = turrentFiles[i].piece_len;
							json_data1["last_piece_len"] = turrentFiles[i].last_piece_len;
							json_data1["num_pieces"] = turrentFiles[i].num_pieces;
							json_data1["md5_file_pieces"] = turrentFiles[i].md5_file_pieces;

							std::string json_string = json_data1.dump();

							data.push_back(json_string);
								
							tcp_helper.sendMsgCode(GIVE_ROOM_TURRENT_FILES, data);

							buffer = tcp_helper.recvMsg();
							answerCode = tcp_helper.getAnswer(buffer);

							if (answerCode != SUCCESS_CODE)
							{
								break;
							}

						}
						data.clear();
						tcp_helper.sendMsgCode(SUCCESS_CODE, data);

					}
					else
					{
						tcp_helper.sendMsgError("there is no uploaded turrent files");
					}
				}
				else
				{
					tcp_helper.sendMsgError("the user is not in any room");
				}

				break;

			default:
				//need to send to the client that the msg code not valid
				tcp_helper.sendMsgError("msg code not valid");
				break;
			}



		}



		// Closing the socket (in the level of the TCP protocol)
		closesocket(clientSocket); 
	}
	catch (const std::exception& e)
	{
		if (clientRoom != nullptr)
		{
			this->_roomManager->leaveRoom(clientRoom->getName(), Client);

		}
		Client = nullptr;
		closesocket(clientSocket);
		cout << "error in the socket connection (client disconnected)" << endl;
	}
}

void Server::give_all_rooms(TCP_Helper tcp_helper)
{
	vector<Room*> rooms;

	rooms = this->_roomManager->getRooms();

	// Get room names from the vector of Room pointers

	if (rooms.empty() == false)
	{
		std::vector<std::pair<std::string,std::string>> roomsInfo;

		for (auto it = rooms.begin(); it != rooms.end(); it++)
		{
			roomsInfo.push_back(make_pair((*it)->getName(), to_string((*it)->getStatus())));
		}
		nlohmann::json jsonArray_names;
		jsonArray_names["rooms"] = roomsInfo;

		// Serialize the JSON array to a string
		std::string jsonString_names = jsonArray_names.dump();

		vector<string> data;
		data.push_back(jsonString_names);

		tcp_helper.sendMsgCode(SUCCESS_CODE, data);
	}
	else
	{
		tcp_helper.sendMsgSuccess();
	}
}

void Server::give_all_users_in_room(TCP_Helper tcp_helper, Room* clientRoom)
{
	vector<client*> clients = clientRoom->getClients();
	map<string, string> clientsPublicKey;
	//TODO: send sorted list

	if (clients.empty() == false)
	{
		std::vector<std::string> clientUDPEndPoints;
		std::vector<std::string> clientsNames;

		for (auto it = clients.begin(); it != clients.end(); it++)
		{
			clientUDPEndPoints.push_back((*it)->getUDPEndpoint());
			clientsNames.push_back((*it)->getName());
		}
		nlohmann::json jsonArray_names;
		jsonArray_names["clients_UDP_endpoints"] = clientUDPEndPoints;
		jsonArray_names["clientsNames"] = clientsNames;
		jsonArray_names["isStarted"] = clientRoom->getStatus();

		// Serialize the JSON array to a string
		std::string jsonString_endPoints = jsonArray_names.dump();

		vector<string> data;
		data.push_back(jsonString_endPoints);

		tcp_helper.sendMsgCode(SUCCESS_CODE, data);
	}
	else
	{
		tcp_helper.sendMsgSuccess();
	}
}

void Server::give_users_public_key(TCP_Helper tcp_helper, Room* clientRoom)
{
	vector<client*> clients = clientRoom->getClients();
	map<string, string> clientsPublicKey;


	if (clients.empty() == false)
	{
		for (auto it = clients.begin(); it != clients.end(); it++)
		{
			clientsPublicKey[(*it)->getUDPEndpoint()] = (*it)->getEncryptPublicKey();
		}
		nlohmann::json jsonArray_names;
		jsonArray_names["clientsPublicKey"] = clientsPublicKey;

		// Serialize the JSON array to a string
		std::string jsonString_endPoints = jsonArray_names.dump();

		vector<string> data;
		data.push_back(jsonString_endPoints);

		tcp_helper.sendMsgCode(SUCCESS_CODE, data);
	}
	else
	{
		tcp_helper.sendMsgSuccess();
	}
}

bool Server::create_room(string json_data, TCP_Helper tcp_helper, client* Client)
{
	string roomName = "";
	string roomPassword = "";
	string buffer;
	string adminPublicKey;

	try {
		nlohmann::json json_data1 = nlohmann::json::parse(json_data);
		roomName = json_data1["room_name"];
		roomPassword = json_data1["password"];
	}
	catch (const std::exception& e) {
		std::cerr << "Failed to deserialize JSON data: " << e.what() << std::endl;
	}

	if (this->_roomManager->createRoom(roomName, roomPassword, Client))
	{
		Room* room = this->_roomManager->getRoom(roomName);
		vector<string> data;
		data.push_back("UDP_password");
		data.push_back(room->getUDP_password());
		data.push_back("room_P");
		data.push_back(to_string(room->getP()));
		data.push_back("room_G");
		data.push_back(to_string(room->getG()));
		tcp_helper.sendMsgCode(SUCCESS_CODE, data);

		buffer = tcp_helper.recvMsg();

		try {
			nlohmann::json json_data1 = nlohmann::json::parse(buffer);
			buffer = json_data1["data"];
			json_data1 = nlohmann::json::parse(buffer);
			adminPublicKey = json_data1["adminPublicKey"];
		}
		catch (const std::exception& e) {
			cout <<  "HUGE ERROR: create room";
			return false;
		}

		Client->setPublicKey(adminPublicKey);

		tcp_helper.sendMsgSuccess();

		return true;
	}
	else
	{
		tcp_helper.sendMsgError("room name already exist");
		return false;
	}
}

bool Server::joinRoom(string json_data, TCP_Helper tcp_helper,client* client)
{
	string roomName;
	string password;
	string UDP_password;
	string answer;
	string buffer;
	string clientPublicKey;

	bool findRoom = false;
	bool passwordMatch = false;

	try {
		nlohmann::json json_data1 = nlohmann::json::parse(json_data);
		roomName = json_data1["room_name"];
		password = json_data1["password"];
		UDP_password = json_data1["UDP_password"];
	}
	catch (const std::exception& e) {
		std::cerr << "Failed to deserialize JSON data: " << e.what() << std::endl;
	}

	answer = this->_roomManager->joinRoom(roomName, password, UDP_password, client);

	
	if (answer == "success")
	{
		Room* room = this->_roomManager->getRoom(roomName);

		vector<string> data;
		data.push_back("room_P");
		data.push_back(to_string(room->getP()));
		data.push_back("room_G");
		data.push_back(to_string(room->getG()));
		data.push_back("adminPublicKey");
		data.push_back(room->getAdmin()->getEncryptPublicKey());
		data.push_back("clientUDP_endpoint");
		data.push_back(client->getUDPEndpoint());
		//send success code
		tcp_helper.sendMsgCode(SUCCESS_CODE,data);


		buffer = tcp_helper.recvMsg();

		try {
			nlohmann::json json_data1 = nlohmann::json::parse(buffer);
			buffer = json_data1["data"];
			json_data1 = nlohmann::json::parse(buffer);
			clientPublicKey = json_data1["PublicKey"];
		}
		catch (const std::exception& e) {
			cout << "HUGE ERROR: join room";
			return false;
		}

		client->setPublicKey(clientPublicKey);

		return true;
	}
	else
	{
		//send to the client error msg
		tcp_helper.sendMsgError(answer);
		return false;
	}

}

void Server::joinRoomCheck(string json_data, TCP_Helper tcp_helper)
{
	string roomName;
	string password;

	bool findRoom = false;
	bool passwordMatch = false;

	try {
		nlohmann::json json_data1 = nlohmann::json::parse(json_data);
		roomName = json_data1["room_name"];
		password = json_data1["password"];
	}
	catch (const std::exception& e) {
		std::cerr << "Failed to deserialize JSON data: " << e.what() << std::endl;
	}

	Room* clientRoom = nullptr;

	for (const auto& room : this->_roomManager->getRooms()) {
		if (room->getName() == roomName) {
			findRoom = true;
			if (room->getPassword() == password)
			{
				passwordMatch = true;
				clientRoom = room;
			}
		}
	}

	if (findRoom)
	{
		if (passwordMatch)
		{
			if (clientRoom->getStatus() == 0)
			{
				vector<string> data;
				data.push_back("adminUDP_endpoint");
				data.push_back(clientRoom->getAdmin()->getUDPEndpoint());
				//send success code
				tcp_helper.sendMsgCode(SUCCESS_CODE,data);
			}
			else
			{
				//send to the client that the password incorrect
				tcp_helper.sendMsgError("the room already running");
			}
		}
		else
		{
			//send to the client that the password incorrect
			tcp_helper.sendMsgError("the password incorrect");
		}
	}
	else
	{
		//send to the client that the room doesnt exist
		tcp_helper.sendMsgError("the room doesnt exist");
	}
}

void Server::uploadTurrentFile(TCP_Helper tcp_helper,Room* clientRoom,client* Client,string json_data)
{
	int numOfTurrents = 0;
	int receiveCode = 0;
	string buffer;
	
	turrent_file turrentFile;
	try {
		nlohmann::json json_data1 = nlohmann::json::parse(json_data);
		turrentFile.file_name = json_data1["file_name"];
		turrentFile.file_len = json_data1["file_len"];
		turrentFile.piece_len = json_data1["piece_len"];
		turrentFile.last_piece_len = json_data1["last_piece_len"];
		turrentFile.num_pieces = json_data1["num_pieces"];
		turrentFile.md5_file_pieces = json_data1["md5_file_pieces"];
	}
	catch (const std::exception& e) {
		std::cerr << "Failed to deserialize JSON data: " << e.what() << std::endl;
	}

	bool success = clientRoom->addTorrentFile(turrentFile, Client);

	if (success)
	{
		tcp_helper.sendMsgSuccess();
	}
	else
	{
		tcp_helper.sendMsgError("only the room admin can upload file");
	}
}

void Server::wantPieceFromAnotherClient(TCP_Helper tcp_helper, Room* clientRoom, client* Client, string json_data)
{

	vector<pair<string, vector<int>>> otherReceivePieces;
	vector<pair<string, vector<int>>> clientReceivePieces;
	vector<pair<string, vector<int>>> requestToSendPieces;

	string clientEndpoint;
	string otherPublicKey;
	string buffer;
	client* otherClient = nullptr;
	int pieceCount = 0;
	
	try {
		nlohmann::json json_data1 = nlohmann::json::parse(json_data);
		clientEndpoint = json_data1["clientEndpoint"];
	}
	catch (const std::exception& e) {
		std::cerr << "Failed to deserialize JSON data: " << e.what() << std::endl;
		tcp_helper.sendMsgError("");
		return;
	}

	if (clientEndpoint == Client->getUDPEndpoint())
	{
		tcp_helper.sendMsgError("you cant request pieces from yourself");
		return;
	}
	else
	{
		//mutex
		vector<turrent_file> turrents = clientRoom->getTurrentFiles();
		//mutex


		//get received pieces of the other client
		for (auto& it_client : clientRoom->getClients())
		{
			if ((it_client)->getUDPEndpoint() == clientEndpoint)
			{
				otherClient = (it_client);
				otherPublicKey = (it_client)->getEncryptPublicKey();

				for (auto& it_turrent : turrents)
				{
					pair<string, vector<int>> turrentPieces;
					turrentPieces.first = (it_turrent).file_name;
					turrentPieces.second = (it_client)->getReceivdPieces((it_turrent).file_name);
					otherReceivePieces.push_back(turrentPieces);

				}
				break;
			}
		}

		//get received pieces of this client
		for (auto& it_turrent : turrents)
		{
			pair<string, vector<int>> turrentPieces;
			turrentPieces.first = (it_turrent).file_name;
			turrentPieces.second = Client->getReceivdPieces((it_turrent).file_name);
			clientReceivePieces.push_back(turrentPieces);
		}
		
		for (int turrent_index = 0; turrent_index < turrents.size(); turrent_index++)
		{
			vector<int> otherPieces = otherReceivePieces[turrent_index].second;
			vector<int> clientPieces = clientReceivePieces[turrent_index].second;

			vector<int> sendPiecesOfTurrent;
			

			for (int i = 0; i < otherPieces.size(); i++)
			{
				if (pieceCount == 50)
				{
					break;
				}


				if (std::find(clientPieces.begin(), clientPieces.end(), otherPieces[i]) == clientPieces.end())
				{
					sendPiecesOfTurrent.push_back(otherPieces[i]);
					pieceCount++;
				}
			}

			if (sendPiecesOfTurrent.size() != 0)
			{
				requestToSendPieces.push_back(make_pair(turrents[turrent_index].file_name, sendPiecesOfTurrent));
			}

			if (pieceCount == 50)
			{
				break;
			}

		}


		


		////send the pieces the other client have
		//nlohmann::json json_msg;
		//json_msg["code"] = SUCCESS_CODE;
		//
		//nlohmann::json json_data;
		//json_data["otherPublicKey"] = otherPublicKey;
		//json_data["receivePieces"] = receivePieces;

		//json_msg["data"] = json_data.dump();
		//std::string json_string = json_msg.dump();
		//tcp_helper.sendMsg(json_string);

		//buffer = tcp_helper.recvMsg();
		
		//vector<pair<string, vector<int>>> wantedPiece;

		//try {
		//	nlohmann::json json_data1 = nlohmann::json::parse(buffer);
		//	//wantedPiece = json_data1["wantedPieces"];
		//	buffer = json_data1["data"];
		//	json_data1 = nlohmann::json::parse(buffer);
		//	wantedPiece = json_data1["wantedPieces"];
		//}
		//catch (const std::exception& e) {
		//	std::cerr << "Failed to deserialize JSON data: " << e.what() << std::endl;
		//}

		//if (wantedPiece.size() == 0) //if the client dont want any piece from the other client
		//{
		//	return;
		//}
		TCP_Helper* otherTcpHelper = otherClient->getTcpHelper();

		if (otherClient == nullptr)
		{
			return;
		}
		
		if (pieceCount != 0)
		{
			nlohmann::json json_msg2;
			json_msg2["code"] = REQUEST_PIECES_FROM_CLIENT;

			nlohmann::json json_data2;
			json_data2["clientEndpoint"] = Client->getUDPEndpoint();
			json_data2["wantedPieces"] = requestToSendPieces;

			json_msg2["data"] = json_data2.dump();
			string json_string = json_msg2.dump();
			otherTcpHelper->sendMsg(json_string);
		}

		
	}
}

structMsg Server::jsonMsg(string msg)
{
	structMsg msg_info;
	try {
		nlohmann::json json_data = nlohmann::json::parse(msg);
		msg_info.code = json_data["code"];
		msg_info.json_data = json_data["data"];
	}
	catch (const std::exception& e) {
		std::cerr << "Failed to deserialize JSON data: " << e.what() << std::endl;
	}
	return msg_info;
}

void Server::pieceUpdate(TCP_Helper tcp_helper, Room* clientRoom, client* Client, string json_data)
{
	vector<pair<string, vector<int>>> receivedPieces;

	try {
		nlohmann::json json_data1 = nlohmann::json::parse(json_data);
		receivedPieces = json_data1["receivedPieces"];
	}
	catch (const nlohmann::json::exception& e) {
		std::cerr << "Failed to deserialize JSON data: " << e.what() << std::endl;
	}

	for (auto it = receivedPieces.begin(); it != receivedPieces.end(); it++)
	{
		for (auto it2 = (*it).second.begin(); it2 != (*it).second.end(); it2++)
		{
			Client->addPiece((*it).first,(*it2));
		}
	}
}

std::vector<int> findMissingPieces(const std::vector<std::unordered_set<int>>& piecesList, turrent_file turrent)
{
	int pieces = turrent.num_pieces;
	std::unordered_set<int> turrentPieces;
	for (int i = 0; i < pieces; i++)
	{
		turrentPieces.insert(i);
	}


	// Combine all pieces into a single set
	std::unordered_set<int> allPieces;
	for (const auto& pieces : piecesList) {
		allPieces.insert(pieces.begin(), pieces.end());
	}

	std::vector<int> sortedTurrentPieces(turrentPieces.begin(), turrentPieces.end());
	std::sort(sortedTurrentPieces.begin(), sortedTurrentPieces.end());

	std::vector<int> sortedAllPieces(allPieces.begin(), allPieces.end());
	std::sort(sortedAllPieces.begin(), sortedAllPieces.end());

	// Find missing pieces
	std::vector<int> missingPieces;
	std::set_difference(sortedTurrentPieces.begin(), sortedTurrentPieces.end(), sortedAllPieces.begin(), sortedAllPieces.end(), std::inserter(missingPieces, missingPieces.begin()));

	return missingPieces;
}

template<typename T>
std::vector<T> find_non_common_elements(const std::vector<std::vector<T>>& lists) {
	if (lists.empty()) {
		return {};  // Return an empty vector if no lists are provided
	}

	std::unordered_set<T> common_elements(lists[0].begin(), lists[0].end());

	for (size_t i = 1; i < lists.size(); ++i) {
		std::unordered_set<T> current_set(lists[i].begin(), lists[i].end());

		// Find the intersection of the current set with common elements
		std::unordered_set<T> temp_set;
		for (const auto& elem : current_set) {
			if (common_elements.find(elem) != common_elements.end()) {
				temp_set.insert(elem);
			}
		}

		// Update common elements with the intersection
		common_elements = temp_set;
	}

	std::vector<T> non_common_elements;

	for (const auto& lst : lists) {
		std::vector<T> temp_result;
		std::copy_if(lst.begin(), lst.end(),
			std::back_inserter(temp_result),
			[&common_elements](const T& elem) {
				return common_elements.find(elem) == common_elements.end();
			});

		// Append temp_result to non_common_elements
		non_common_elements.insert(non_common_elements.end(), temp_result.begin(), temp_result.end());
	}

	return non_common_elements;
}

void Server::sendMissingPieces(TCP_Helper tcp_helper,string msg_data, Room* clientRoom)
{

	//mutex
	vector<client*> clients = clientRoom->getClients();
	vector<turrent_file> turrents = clientRoom->getTurrentFiles();
	//mutex
	vector<int> clientPieces;
	vector<int> piecesMissing;

	vector<pair<string, vector<int>>> resualt;

	int temp;

	/*std::vector<std::unordered_set<int>> listOfClientPieces;
		
	for (auto& turrent : turrents)
	{
		listOfClientPieces.clear();

		for (auto& it_client : clients)
		{
			clientPieces = it_client->getReceivdPieces(turrent.file_name);
			std::unordered_set<int> setOfInts(clientPieces.begin(), clientPieces.end());
			listOfClientPieces.push_back(setOfInts);
		}

		piecesMissing = findMissingPieces(listOfClientPieces, turrent);
		if (piecesMissing.empty() == false)
		{
			resualt.push_back(make_pair(turrent.file_name, piecesMissing));
		}
	}*/

	
	
	

	std::vector<std::vector<int>> listOfClientPieces;
	vector<int> missingPieces;

	for (auto& turrent : turrents)
	{
		listOfClientPieces.clear();
		piecesMissing.clear();

		for (auto& it_client : clients)
		{
			clientPieces = it_client->getReceivdPieces(turrent.file_name);
			listOfClientPieces.push_back(clientPieces);
		}
		

		piecesMissing = (vector<int>)find_non_common_elements(listOfClientPieces);


		//for (auto& pieceNum : turrent.md5_file_pieces)
		//{
		//	temp = pieceNum.second;
		//	// Check if all vectors contain the number 1
		//	bool allContainPiece = std::all_of(listOfClientPieces.begin(), listOfClientPieces.end(), [temp](const std::vector<int>& v) {
		//		return std::find(v.begin(), v.end(), temp) != v.end();
		//		});

		//	if (allContainPiece == false)
		//	{
		//		piecesMissing.push_back(pieceNum.second);
		//	}
		//}

		if (piecesMissing.empty() == false)
		{
			resualt.push_back(make_pair(turrent.file_name, piecesMissing));
		}
	}
	nlohmann::json json_msg;
	json_msg["code"] = GIVE_MISSING_PIECES;

	nlohmann::json json_data;
	json_data["missingPieces"] = resualt;
	
	json_msg["data"] = json_data.dump();
	string json_string = json_msg.dump();
	tcp_helper.sendMsg(json_string);
}







