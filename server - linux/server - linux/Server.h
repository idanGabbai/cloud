#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <thread>
#include <WinSock2.h>
#include <Windows.h>
#include <mutex>
#include "Helper.h"
#include <fstream>
#include "RoomManager.h"
#include "json.hpp"
#include <algorithm>
#include <unordered_set>
#include <set>
#define NUM_OF_ID_MSG_SIZE 4
#define UDP_SOCKET_PORT 4545

#define KEEP_ALIVE_MSG "0000"

using namespace std;

enum MessageType : int
{
	LOGIN_CODE = 10,
	GIVE_ALL_ROOMS = 11,
	JOIN_ROOM = 12,
	JOIN_ROOM_CHECK = 16,
	CREATE_ROOM = 13,
	GIVE_USERS_IN_ROOM = 14,
	LOGOUT = 99,
	PIECE_UPDATE = 20,
	WANT_PIECE_FROM_ANOTHER_CLIENT = 21,
	STAY_ALIVE = 25,
	GIVE_ME_ASSISTENT = 26,
	UPLOAD_TORRENT_FILE = 30,
	LEAVE_ROOM = 15,
	GIVE_ROOM_TURRENT_FILES = 31,
	REMOVE_ROOM_TURRENT_FILES = 32,
	START_ROOM = 40,
	REQUEST_PIECES_FROM_CLIENT = 42,
	GIVE_MISSING_PIECES = 43,
	GIVE_USERS_PUBLIC_KEY = 45,
	ROOM_CLOSED = 50,
	STOP_ROOM = 55
};

enum MessageCode : int
{
	ERROR_CODE = 500,
	SUCCESS_CODE = 100
};

typedef struct structMsg
{
	int code;
	string json_data;
}structMsg;

class Server
{
public:
	Server();
	~Server();
	void serve(int port);
	void acceptClient();

private:
	SOCKET _serverSocket;
	roomManager* _roomManager;
	map<int, string> clientsUDP;

	void receiveUDP_EndPoints();

	void give_all_rooms(TCP_Helper tcp_helper);
	void give_all_users_in_room(TCP_Helper tcp_helper,Room* clientRoom);
	void give_users_public_key(TCP_Helper tcp_helper, Room* clientRoom);
	bool create_room(string json_data, TCP_Helper tcp_helper, client* Client);
	void clientHandler(SOCKET clientSocket,int id);
	bool joinRoom(string json_data, TCP_Helper tcp_helper,client* client);
	void joinRoomCheck(string json_data, TCP_Helper tcp_helper);
	void uploadTurrentFile (TCP_Helper tcp_helper, Room* clientRoom, client* Client, string json_data);
	void wantPieceFromAnotherClient(TCP_Helper tcp_helper, Room* clientRoom, client* Client, string json_data);
	structMsg jsonMsg(string msg);
	void pieceUpdate(TCP_Helper tcp_helper, Room* clientRoom, client* Client, string json_data);
	void sendMissingPieces(TCP_Helper tcp_helper,string msg_data, Room* clientRoom);
};

