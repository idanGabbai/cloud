#include "Client.h"



client::client(std::string name, std::string UDP_endPoint,TCP_Helper* tcp_helper)
{
	this->_name = name;
	this->_receivedPieces.clear();
	this->_UDP_endPoint = UDP_endPoint;
	this->_encryptPublicKey = "";
	this->tcp_helper = tcp_helper;
}

void client::addTorrent(std::string turrentName)
{

	piecesNumReceivedInTurrent turrent;
	turrent.file_name = turrentName;
	turrent.received_pieces_num.clear();
	this->_receivedPieces.push_back(turrent);
}

void client::addPiece(std::string turrentName, int pieceNum)
{
	bool valid = true;
	for (auto it = this->_receivedPieces.begin(); it != this->_receivedPieces.end(); it++)
	{
		if ((*it).file_name == turrentName)
		{
			for (auto it2 = (*it).received_pieces_num.begin(); it2 != (*it).received_pieces_num.end(); it2++)
			{
				if (pieceNum == (*it2))
				{
					valid = false;
					break;
				}
			}

			if (valid)
			{
				(*it).received_pieces_num.push_back(pieceNum);
			}

			break;
		}
	}
}

std::vector<int> client::getReceivdPieces(std::string turrentName)
{
	for (auto it = this->_receivedPieces.begin(); it != this->_receivedPieces.end(); it++)
	{
		if (turrentName == (*it).file_name)
		{
			return (*it).received_pieces_num;
		}
	}
	return std::vector<int>();
}

std::string client::getEncryptPublicKey()
{
	return this->_encryptPublicKey;
}

std::string client::getUDPEndpoint()
{
	return this->_UDP_endPoint;
}

std::string client::getName()
{
	return this->_name;
}

void client::setPublicKey(std::string publicKey)
{
	this->_encryptPublicKey = publicKey;
}

TCP_Helper* client::getTcpHelper()
{
	return this->tcp_helper;
}
