#pragma once
#include "Client.h"
#include <ctime>
class Room
{
public:
	Room(client* admin,std::string name,std::string password);
	~Room();

	void addClient(client* client);
	void removeClient(client* client);
	bool addTorrentFile(turrent_file torrentFile,client* client);
	void removeTurrentFiles();
	void startRoom();
	void stopRoom();

	//getters
	std::string getPassword() const;
	std::string getName() const;
	std::vector<client*> getClients() const;
	client* getAdmin() const;
	std::vector<turrent_file> getTurrentFiles();
	std::string getUDP_password() const;
	bool getStatus() const;
	long long getP();
	long long getG();


private:
	std::string _name;
	client* _admin;
	std::vector<client*> _clients;
	std::string _password;
	std::string _UDP_password;
	std::vector<turrent_file> _torrentsFile;

	bool _status;

	long long _G;
	long long _P;
};
