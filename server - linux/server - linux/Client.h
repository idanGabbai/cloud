#pragma once
#include <string>
#include <vector>
#include <map>
#include "Helper.h"

typedef struct turrent_file
{
	std::string file_name;
	int file_len;
	int piece_len;
	int last_piece_len;
	int num_pieces;
	std::multimap<std::string, int> md5_file_pieces;
}turrent_file;

typedef struct piecesNumReceivedInTurrent
{
	std::string file_name;
	std::vector<int> received_pieces_num;
} piecesNumReceivedInTurrent;


class client
{
public:
	client(std::string name, std::string UDP_endPoint, TCP_Helper* tcp_helper);
	~client();

	void addTorrent(std::string torrentName);
	void addPiece(std::string torrentName, int pieceNum);
	std::vector<int> getReceivdPieces(std::string torrentName);
	std::string getEncryptPublicKey();
	std::string getUDPEndpoint();
	std::string getName();
	void setPublicKey(std::string publicKey);
	TCP_Helper* getTcpHelper();

private:
	std::string _name;
	std::string _UDP_endPoint;
	std::string _encryptPublicKey;
	TCP_Helper* tcp_helper;
	std::vector<piecesNumReceivedInTurrent> _receivedPieces;
};

