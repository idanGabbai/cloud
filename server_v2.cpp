#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <map>
#include <vector>

static int PORT = 6668;

int serverSocket;

std::map<int, sockaddr_in> current;
std::vector<sockaddr_in> clients;

std::string NormalizedIPString(sockaddr_in addr) {
    char host[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(addr.sin_addr), host, INET_ADDRSTRLEN);

    uint16_t port = ntohs(addr.sin_port);

    return std::string(host) + ":" + std::to_string(port);
}

void SendResponse(sockaddr_in addr, sockaddr_in receiver) {
    std::string msg = NormalizedIPString(addr);
    sendto(serverSocket, msg.c_str(), msg.length(), 0, (struct sockaddr*)&receiver, sizeof(receiver));
}

void SendMsg(std::string msg, sockaddr_in receiver) {
    sendto(serverSocket, msg.c_str(), msg.length(), 0, (struct sockaddr*)&receiver, sizeof(receiver));
}

int main() {
    sockaddr_in sockAddr;
    sockAddr.sin_port = htons(PORT);
    sockAddr.sin_family = AF_INET;
    sockAddr.sin_addr.s_addr = INADDR_ANY;

    serverSocket = socket(AF_INET, SOCK_DGRAM, 0);

    if (bind(serverSocket, (struct sockaddr*)&sockAddr, sizeof(sockAddr)) == -1) {
        return 0;
    }

    int val = 64 * 1024;
    setsockopt(serverSocket, SOL_SOCKET, SO_SNDBUF, &val, sizeof(val));
    setsockopt(serverSocket, SOL_SOCKET, SO_RCVBUF, &val, sizeof(val));

    while (true) {
        sockaddr_in clientAddr;
        socklen_t clientSize = sizeof(clientAddr);

        char buffer[200000];
        int bufferLength = 200000;

        int iResult = recvfrom(serverSocket, buffer, bufferLength, 0, (struct sockaddr*)&clientAddr, &clientSize);

        if (iResult > 0 && (buffer[0] == 'T' || buffer[0] == '{')) {
            for (auto it = clients.begin(); it != clients.end(); it++) {
                    SendMsg(buffer, *it);
            }
        }
        else if (iResult > 0) {
            try {
                clients.push_back(clientAddr);
                int id = stoi(std::string(buffer, buffer + iResult));

                if (current.find(id) != current.end()) {
                    sockaddr_in other = current[id];

                    SendResponse(other, clientAddr);
                    SendResponse(clientAddr, other);

                    std::cout << "Linked" << std::endl << "   ID: " << id << std::endl
                        << "   Endpoint1: " << NormalizedIPString(clientAddr) << std::endl
                        << "   Endpoint2: " << NormalizedIPString(other) << std::endl << std::endl;

                    current.erase(id);
                }
                else {
                    current.insert(std::make_pair(id, clientAddr));

                    std::cout << "Registered" << std::endl << "   ID: " << id << std::endl
                        << "   Endpoint: " << NormalizedIPString(clientAddr) << std::endl << std::endl;
                }
            }
            catch (std::invalid_argument& e) {}
            catch (std::out_of_range& e) {}
            catch (...) {}
        }
    }

    close(serverSocket);
    return 0;
}
